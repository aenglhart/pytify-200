# pytify-200

This is a small Python project using data provided Spotify. The TOP 200 songs of every day are collected and stored in .csv files. The song data is extended by further data which can be accessed using the  <b>spotipy</b> library.


## TODO // Progress Control:

      
 ✓ Download Charts
 ✓ Extend Data by Features

 o Include Analytics         
 o GUI


## Convert ipynb into py
Install ipython libraries (Of course, you can skip if already installed.)
> pip install ipython
> pip install nbconvert

Convert single file
> ipython nbconvert — to script abc.ipynb

You can have abc.py
Convert multi files
> ipython nbconvert — to script abc.ipynb def.ipynb

abc.py, def.py
> ipython nbconvert — to script *.ipynb

abc.py, def.py
All done!
--> instructions are from: https://medium.com/@researchplex/the-easiest-way-to-convert-jupyter-ipynb-to-python-py-912e39f16917 

